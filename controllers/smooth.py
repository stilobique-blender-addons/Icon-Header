import bpy
import math


class SmoothShading(bpy.types.Operator):
    """Activate the Auto Smooth with an 45° angle"""
    bl_idname = "object.smooth_shading"
    bl_label = "Advanced Smooth Shading"
    bl_options = {'REGISTER', 'UNDO'}

    shade: bpy.props.BoolProperty(
        name='Activate Shading',
        description='Shade state',
        default=True,
    )

    angle: bpy.props.FloatProperty(
        name='Set Angle',
        description='Angle to control the smooth shading',
        subtype='ANGLE',
        default=math.degrees(0.785),
        min=0,
        max=math.degrees(2.181)
    )

    @classmethod
    def poll(cls, context):
        return context.object or context.edit_object is not None

    def execute(self, context):
        selected_obj = context.selected_objects
        data = bpy.data

        for obj in selected_obj:
            if obj.type == "MESH":
                for face in data.objects[obj.name].data.polygons:
                    face.use_smooth = self.shade

                mesh_name = data.objects[obj.name].data.name
                data.meshes[mesh_name].auto_smooth_angle = self.angle
                if self.angle > 0:
                    use_angle = self.shade
                else:
                    use_angle = not self.shade

                data.objects[obj.name].data.use_auto_smooth = use_angle

        return {'FINISHED'}

    def invoke(self, context, event):
        user_preferences = bpy.context.preferences
        addon_setting = user_preferences.addons['Icon-Header'].preferences
        self.angle = addon_setting.smoothing_intensity

        return self.execute(context)
