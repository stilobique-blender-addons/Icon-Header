import bpy


class RetopoShading(bpy.types.Operator):
    """Setup the viewport to a retopo shading, activate the X-ray to the 
    selected object and the Hidden Wire on"""
    bl_idname = "object.retopo_shading"
    bl_label = "Retopo shading"

    def execute(self, context):
        sltd_obj = context.selected_objects
        area = None
        for a in bpy.data.window_managers[0].windows[0].screen.areas:
            if a.type == 'VIEW_3D':
                area = a
                break

        if area:
            space = area.spaces[0].overlay
        else:
            space = bpy.context.space_data.overlay

        if space.show_retopology:
            space.show_retopology = False
            for obj in sltd_obj:
                if obj.type == "MESH" or "CURVE":
                    context.active_object.show_in_front = False

        else:
            space.show_retopology = True
            for obj in sltd_obj:
                if obj.type == "MESH" or "CURVE":
                    context.active_object.show_in_front = True

        return {'FINISHED'}
