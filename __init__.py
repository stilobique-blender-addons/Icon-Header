import bpy

from .models import grid
from .views import header, settings
from .controllers import checker, cursor_uv, grid, location, retopo, smooth

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Icon Header",
    "author": "stilobique",
    "version": (1, 2, 1),
    "blender": (2, 80, 0),
    "location": "3D View > Header",
    "description": "Various new operator.",
    "warning": "",
    "wiki_url": "https://gitlab.com/stilobique/Icon-Header/wikis/home",
    "category": "3D View",
    "tracker_url": "https://gitlab.com/stilobique/Icon-Header/issues",
}


classes = (
    # Models,
    models.grid.GridData,
    # View,
    header.STILOBIQUE_PT_viewport_sub,
    settings.IconHeaderSetting,
    # Controller,
    checker.UVChecker,
    cursor_uv.ResetCursor,
    grid.GridControl,
    location.CenterPivotMeshObj,
    retopo.RetopoShading,
    smooth.SmoothShading,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.VIEW3D_MT_editor_menus.append(header.STILOBIQUE_HT_viewport)
    bpy.types.Scene.icon_header = bpy.props.PointerProperty(type=models.grid.GridData)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)

    bpy.types.VIEW3D_MT_editor_menus.remove(header.STILOBIQUE_HT_viewport)
    del bpy.types.Scene.icon_header
