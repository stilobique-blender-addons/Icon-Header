import bpy


def update_grid(self, context):
    bpy.ops.view.grid_control()
    return


class GridData(bpy.types.PropertyGroup):
    grid_activate: bpy.props.BoolProperty(
        name="X Axis",
        description="State about the grid floor",
        default=True,
        update=update_grid
    )